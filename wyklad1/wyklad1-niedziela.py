class Samochod:
#                PEP8
    def __init__(self, marka, kolor, akumulator):
        self.marka = marka
        self.kolor = kolor
        self.akumulator = akumulator

    def __str__(self):
        informacje = "marka: {}; kolor: {}; akumulator: {}".format(
            self.marka,
            self.kolor,
            self.akumulator
        )
        return informacje

    def __eq__(self, other):
        return (self.marka == other.marka
                and self.kolor == other.kolor)

    def start(self):
        if self.__czy_mozna_wystartowac():
            print("start:", self.kolor, self.marka)
        else:
            print("brak prądu")

#       PEP8
    def __czy_mozna_wystartowac(self):
        return self.akumulator > 30

samochod1 = Samochod("fiat", "czerwony", 20)
samochod2 = Samochod("fiat", "czerwony", 60)

if samochod1 == samochod2:  # samochod1.__eq__(samochod2)
    print("samochody są takie same")
else:
    print("samochody są różne")

print("samochod1:", samochod1)

informacje = str(samochod2)
print("informacje:", informacje)

samochod1.start()
samochod2.start()

# funkcja prywatna - nie można użyć
# print("samochod1: ", samochod1.__czy_mozna_wystartowac())
