class Samochod:
    def __init__(self, marka, kolor, akumulator):
        self.marka = marka
        self.kolor = kolor
        self.akumulator = akumulator

    def __eq__(self, inny_samochod):
        return (self.akumulator == inny_samochod.akumulator
                and self.kolor == inny_samochod.kolor
                and self.marka == inny_samochod.marka)

    def __str__(self):
        informacje = "marka: {}; kolor: {}; akumulator: {}".format(
                          self.marka,
                          self.kolor,
                          self.akumulator)
        return informacje

    def start(self):
        if self.__czy_moge_wystartowac():
            print("Startuje", self.marka,
                  "koloru", self.kolor,
                  "poziom akumulatora", self.akumulator)
        else:
            print("Brak prądu")

    def __czy_moge_wystartowac(self):
        return self.akumulator > 30

samochod1 = Samochod("fiat", "czerwony", 0)
samochod2 = Samochod("fiat", "czerwony", 60)

samochod1.start()
samochod2.start()

print("Informacje o samochodzie 1:", samochod1)

informacje = str(samochod1)
print(informacje)

if samochod1 == samochod2:  #-> samochod1.__eq__(samochod2)
    print("są indentyczne")
else:
    print("są różne")
